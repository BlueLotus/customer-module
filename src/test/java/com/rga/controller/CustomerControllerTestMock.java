package com.rga.controller;

import com.google.common.collect.Lists;
import com.rga.controller.CustomerController;
import com.rga.domain.Customer;
import com.rga.exception.ResourceNotFoundException;
import com.rga.exception.UserAlreadyExistException;
import com.rga.service.impl.CustomerServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Carl Lu
 */
public class CustomerControllerTestMock {

    @Mock
    private CustomerServiceImpl customerService;

    private Iterable<Customer> customers;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        initTestData();
    }

    public void initTestData() {
        Customer customer1 = new Customer();
        Customer customer2 = new Customer();
        List<Customer> customers = new ArrayList<Customer>();
        customers.add(customer1);
        customers.add(customer2);
        this.customers = customers;
    }

    @Test
    public void testGetAllCustomers() {
        CustomerController customerController = new CustomerController();
        ReflectionTestUtils.setField(customerController, "customerService", customerService);

        when(customerService.getAllCustomers()).thenReturn(customers);
        ResponseEntity<Iterable<Customer>> allCustomersEntity = customerController.getAllCustomers();
        verify(customerService, times(1)).getAllCustomers();
        assertEquals(HttpStatus.OK, allCustomersEntity.getStatusCode());
        assertEquals(2, Lists.newArrayList(allCustomersEntity.getBody()).size());
    }

    @Test(expected = UserAlreadyExistException.class)
    public void testCreateCustomerWithExistedId() {
        CustomerController customerController = new CustomerController();
        ReflectionTestUtils.setField(customerController, "customerService", customerService);

        String userName = "admin";
        Customer admin = new Customer();
        admin.setUsername(userName);
        admin.setName("Carl");
        admin.setPassword("12345");
        admin.setAdmin(true);

        when(customerService.isUserAlreadyExist(userName)).thenThrow(UserAlreadyExistException.class);
        customerController.createCustomer(admin);
    }

    @Test
    public void testGetCustomerById() {
        CustomerController customerController = new CustomerController();
        ReflectionTestUtils.setField(customerController, "customerService", customerService);

        Long customerId = 1l;
        Customer admin = new Customer();
        admin.setUsername("admin");
        admin.setName("Carl");
        admin.setPassword("admin");
        admin.setAdmin(true);

        when(customerService.getById(customerId)).thenReturn(admin);
        ResponseEntity<?> customerEntity = customerController.getCustomer(customerId);
        verify(customerService, times(2)).getById(customerId);
        assertEquals(HttpStatus.OK, customerEntity.getStatusCode());
        assertEquals(admin.toString(), customerEntity.getBody().toString());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testGetCustomerByWrongId() {
        CustomerController customerController = new CustomerController();
        ReflectionTestUtils.setField(customerController, "customerService", customerService);

        Long customerId = 99999l;

        when(customerService.getById(customerId)).thenThrow(ResourceNotFoundException.class);
        customerController.getCustomer(customerId);
    }

    @Test
    public void testUpdateCustomer() {
        CustomerController customerController = new CustomerController();
        ReflectionTestUtils.setField(customerController, "customerService", customerService);

        Long customerId = 1l;
        Customer customer = new Customer();
        customer.setId(customerId);
        customer.setUsername("customer");
        customer.setName("CarlLu");
        customer.setPassword("customer");
        customer.setAdmin(false);
        customer.setTokenDigest("digest");

        when(customerService.getById(customerId)).thenReturn(customer);
        when(customerService.save(customer)).thenReturn(customer);
        ResponseEntity<Void> customerEntity = customerController.updateCustomer(customer, customerId);
        verify(customerService, times(2)).getById(customerId);
        verify(customerService, times(1)).save(customer);
        assertEquals(HttpStatus.OK, customerEntity.getStatusCode());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testUpdateCustomerWithNonExistentId() {
        CustomerController customerController = new CustomerController();
        ReflectionTestUtils.setField(customerController, "customerService", customerService);

        Customer customer = new Customer();
        Long customerId = 999l;

        when(customerService.getById(customerId)).thenThrow(ResourceNotFoundException.class);
        customerController.updateCustomer(customer, customerId);
    }

    @Test
    public void testDeleteCustomerWithId() {
        CustomerController customerController = new CustomerController();
        ReflectionTestUtils.setField(customerController, "customerService", customerService);

        Customer customer = new Customer();
        Long customerId = 1l;

        when(customerService.getById(customerId)).thenReturn(customer);
        ResponseEntity<Void> deleteResult = customerController.deleteCustomer(customerId);
        verify(customerService, times(1)).getById(customerId);
        assertEquals(HttpStatus.OK, deleteResult.getStatusCode());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testDeleteCustomerWithNonExistentId() {
        CustomerController customerController = new CustomerController();
        ReflectionTestUtils.setField(customerController, "customerService", customerService);

        Customer customer = new Customer();
        Long customerId = 999l;

        when(customerService.getById(customerId)).thenThrow(ResourceNotFoundException.class);
        customerController.deleteCustomer(customerId);
    }

}
