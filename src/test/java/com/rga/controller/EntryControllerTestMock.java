package com.rga.controller;

import com.rga.domain.Customer;
import com.rga.dto.security.LoginResponse;
import com.rga.dto.security.UserLogin;
import com.rga.enums.Roles;
import com.rga.exception.LoginFailedException;
import com.rga.service.CustomerService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Carl Lu
 */
public class EntryControllerTestMock {

    @Mock
    private CustomerService customerService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testLoginSuccessfully() {
        EntryController entryController = new EntryController();
        ReflectionTestUtils.setField(entryController, "customerService", customerService);

        String username = "admin";

        UserLogin login = new UserLogin();
        login.setUsername(username);
        login.setPassword("12345");

        Customer customer = new Customer();

        when(customerService.isLoginSuccessful(login)).thenReturn(true);
        when(customerService.getUserRole(username)).thenReturn(Roles.ADMIN.getRole());
        when(customerService.getByUsername(username)).thenReturn(customer);
        when(customerService.save(customer)).thenReturn(customer);
        ResponseEntity<LoginResponse> result = entryController.login(login);
        verify(customerService, times(1)).isLoginSuccessful(login);
        verify(customerService, times(1)).getUserRole(username);
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test(expected = LoginFailedException.class)
    public void testLoginFailed() {
        EntryController entryController = new EntryController();
        ReflectionTestUtils.setField(entryController, "customerService", customerService);

        String username = "nobody";
        UserLogin login = new UserLogin();
        login.setUsername(username);
        login.setPassword("12345");
        when(customerService.isLoginSuccessful(login)).thenThrow(LoginFailedException.class);
        entryController.login(login);
    }

}
