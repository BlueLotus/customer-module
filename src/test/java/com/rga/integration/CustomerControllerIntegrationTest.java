package com.rga.integration;

import com.rga.RgaCustomerModuleApplication;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * @author Carl Lu
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RgaCustomerModuleApplication.class)
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerControllerIntegrationTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    private String newCustomer = "{\n" +
            "    \"username\": \"kiki\",\n" +
            "    \"password\": \"12345\",\n" +
            "    \"name\": \"kiki\",\n" +
            "    \"admin\": false\n" +
            "}";

    @Before
    public void setup() {
        mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void test1GetAllCustomers() throws Exception {
        String expectedCustomers = "[{\"id\":1,\"username\":\"admin\",\"name\":\"Carl\",\"admin\":true}," +
                                    "{\"id\":2,\"username\":\"ruru\",\"name\":\"RuRu\",\"admin\":false}]";

        mockMvc.perform(get("/rga-customer/customer-service/customers"))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                        .andExpect(content().string(expectedCustomers));
    }

    @Test
    public void test2CreateCustomer () throws Exception {
        mockMvc.perform(post("/rga-customer/customer-service/customer").content(newCustomer).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "http://localhost/rga-customer/customer-service/customer/3"));
    }

    @Test
    public void test3CreateCustomerWithExistedId () throws Exception {
        mockMvc.perform(post("/rga-customer/customer-service/customer").content(newCustomer).contentType(MediaType.APPLICATION_JSON_UTF8));
        mockMvc.perform(post("/rga-customer/customer-service/customer").content(newCustomer).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void test4GetCustomerById() throws Exception {
        String expectedResult = "{\"id\":1,\"username\":\"admin\",\"name\":\"Carl\",\"admin\":true}";

        mockMvc.perform(get("/rga-customer/customer-service/customers/1"))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                        .andExpect(content().string(expectedResult));
    }

    @Test
    public void test5GetCustomerByWrongId() throws Exception {
        mockMvc.perform(get("/rga-customer/customer-service/customers/999"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test6UpdateCustomer() throws Exception {
        String updatedCustomer = "{\n" +
                "  \"id\" : 1,\n" +
                "  \"username\": \"admin\",\n" +
                "  \"password\": \"admin\",\n" +
                "  \"name\": \"CarlLu\",\n" +
                "  \"admin\": true\n" +
                "}";
        mockMvc.perform(put("/rga-customer/customer-service/customers/1").content(updatedCustomer).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    public void test7UpdateCustomerWithNonExistentId() throws Exception {
        String updatedCustomer = "{\n" +
                "  \"id\" : 1,\n" +
                "  \"username\": \"admin\",\n" +
                "  \"password\": \"admin\",\n" +
                "  \"name\": \"CarlLu\",\n" +
                "  \"admin\": true\n" +
                "}";
        mockMvc.perform(put("/rga-customer/customer-service/customers/999").content(updatedCustomer).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test8DeleteCustomerWithId() throws Exception {
        mockMvc.perform(post("/rga-customer/customer-service/customer").content(newCustomer).contentType(MediaType.APPLICATION_JSON_UTF8));
        mockMvc.perform(delete("/rga-customer/customer-service/customers/3"))
                .andExpect(status().isOk());
    }

    @Test
    public void test9DeleteCustomerWithNonExistentId() throws Exception {
        mockMvc.perform(delete("/rga-customer/customer-service/customers/999"))
                .andExpect(status().isNotFound());
    }

}
