package com.rga.dummy;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.rga.RgaCustomerModuleApplication;

/**
 * @author Carl Lu
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RgaCustomerModuleApplication.class)
public class RequestHandlerTest {

    @Autowired
    private RequestHandler requestHandler;

    @Test
    public void testPrototype() {
        Request req1 = Request.getInstance(1l, "R1");
        Request req2 = Request.getInstance(2l, "R2");
        Request req3 = Request.getInstance(3l, null);
        Request req4 = Request.getInstance(4l, "R4");
        Request req5 = Request.getInstance(5l, null);

        requestHandler.handle(req1);
        requestHandler.handle(req2);
        requestHandler.handle(req3);
        requestHandler.handle(req4);
        requestHandler.handle(req5);
    }

}
