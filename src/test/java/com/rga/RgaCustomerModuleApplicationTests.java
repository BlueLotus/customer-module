package com.rga;

import com.rga.integration.CustomerControllerIntegrationTest;
import com.rga.integration.EntryControllerIntegrationTest;
import com.rga.controller.CustomerControllerTestMock;
import com.rga.controller.EntryControllerTestMock;
import com.rga.unit.cryp.AESCryptographyTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ AESCryptographyTest.class
					, CustomerControllerTestMock.class
					, EntryControllerTestMock.class
					, CustomerControllerIntegrationTest.class
					, EntryControllerIntegrationTest.class})
public class RgaCustomerModuleApplicationTests {

}
