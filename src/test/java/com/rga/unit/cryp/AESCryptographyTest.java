package com.rga.unit.cryp;

import com.rga.util.cryp.AESDecryptor;
import com.rga.util.cryp.AESEncryptor;
import com.rga.util.cryp.AESKeyGenerator;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Carl Lu
 */
public class AESCryptographyTest {

    private static byte[] secretKey;
    private static String initialVector;
    private static String testEnvKeyName = AESKeyGenerator.getTestEnvKey();

    @BeforeClass
    public static void before() {
        System.out.println("Test material initializing...\n");
        secretKey = AESKeyGenerator.generateSecretKey();
        initialVector = "0102030405060708";
    }

    @Test
    public void cryptoStringWithAESECBMode() {
        System.out.println("---AES ECB Mode Start---");
        String strToEncrypt = "admin";
        System.out.println("String to be encrypted: " + strToEncrypt);
        AESKeyGenerator.writeKeyToFile(secretKey, testEnvKeyName);
        System.out.println("Secret Key: " + new String(secretKey));
        String strToDecrypt = AESEncryptor.encryptForStringWithECBMode(strToEncrypt, secretKey);
        System.out.println("String after encrypted: " + strToDecrypt);
        String decryptedString = AESDecryptor.decryptForStringWithECBMode(strToDecrypt, AESKeyGenerator.obtainKeyFromFile(testEnvKeyName));
        System.out.println("String after decrypted: " + decryptedString);
        assertEquals(strToEncrypt, decryptedString);
        System.out.println("---AES ECB Mode End---\n");
    }

    @Test
    public void cryptoStringWithAESCBCMode() {
        System.out.println("---AES CBC Mode Start---");
        String strToEncrypt = "ruru";
        System.out.println("String to be encrypted: " + strToEncrypt);
        AESKeyGenerator.writeKeyToFile(secretKey, testEnvKeyName);
        System.out.println("Secret Key: " + new String(secretKey));
        String strToDecrypt = AESEncryptor.encryptForStringWithCBCMode(strToEncrypt, secretKey, initialVector);
        System.out.println("String after encrypted: " + strToDecrypt);
        String decryptedString = AESDecryptor.decryptForStringWithCBCMode(strToDecrypt, AESKeyGenerator.obtainKeyFromFile(testEnvKeyName), initialVector);
        System.out.println("String after decrypted: " + decryptedString);
        assertEquals(strToEncrypt, decryptedString);
        System.out.println("---AES CBC Mode End---\n");
    }

    @Test
    public void keyIOTest() {
        System.out.println("---Key IO functionality test---");
        AESKeyGenerator.writeKeyToFile(secretKey, testEnvKeyName);
        System.out.println("Secret Key: " + new String(secretKey));
        byte[] obtainedKey = AESKeyGenerator.obtainKeyFromFile(testEnvKeyName);
        System.out.println("Obtained Key: " + new String(obtainedKey));
        assertEquals(new String(secretKey), new String(obtainedKey));
        System.out.println("---Key IO functionality test complete---");
    }

}
