package com.rga.dummy;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Carl Lu
 */
@Component
public class RequestHandler {

    @Autowired
    private Validator validator;

    public void handle(Request request) {
        List<String> validationMsgs = validator.validate(request);
        if (!validationMsgs.isEmpty()) {
            validationMsgs.stream().forEach(msg -> System.out.println(msg));
        } else {
            System.out.println("Validation passed for request: " + request.getId());
        }
        System.out.println("Validator: " + validator.toString());
    }

}
