package com.rga.dummy;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * @author Carl Lu
 */
@Component
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "prototype")
public class Validator {

    private List<String> validationMessages = new ArrayList<>();

    public Validator() {}

    public List<String> validate(Request request) {
        if (request.getContent() == null) {
            validationMessages.add("Request: " + request.getId() + " failed (no content).");
        }
        return validationMessages;
    }

}
