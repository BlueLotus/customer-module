package com.rga.config;

import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.rga.filter.JwtFilter;

/**
 * @author Carl Lu
 */
@Configuration
public class WebConfig {

    @Bean
    public FilterRegistrationBean jwtFilter() {
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new JwtFilter());
        registrationBean.addUrlPatterns("/rga-customer/customer-service/*", "/rga-customer/logout");
        return registrationBean;
    }

}
