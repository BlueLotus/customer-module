package com.rga.service.impl;

import com.rga.domain.Customer;
import com.rga.dto.security.UserLogin;
import com.rga.enums.Roles;
import com.rga.exception.LoginFailedException;
import com.rga.repository.CustomerRepository;
import com.rga.service.CustomerService;
import com.rga.util.ApplicationKeyUtil;
import com.rga.util.cryp.AESDecryptor;
import com.rga.util.cryp.AESEncryptor;
import com.rga.util.cryp.AESKeyGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Carl Lu
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    private String initialVector = ApplicationKeyUtil.getInstance().getInitialVectorForCryp();

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public boolean isUserAlreadyExist(String userName) {
        return customerRepository.findByUsername(userName) != null;
    }

    @Override
    public boolean isLoginSuccessful(UserLogin login) {
        String username = login.getUsername();
        if(username == null || !isUserAlreadyExist(username)) {
            throw new LoginFailedException("Invalid username.");
        } else if(!isPasswordCorrect(login)) {
            throw new LoginFailedException("Invalid password.");
        }
        return true;
    }

    @Override
    public String getUserRole(String username) {
        boolean isAdmin = customerRepository.findByUsername(username).isAdmin();
        return isAdmin ? Roles.ADMIN.getRole() : Roles.USER.getRole();
    }

    @Override
    public Customer getById(Long customerId) {
        return customerRepository.findOne(customerId);
    }

    @Override
    public Customer getByUsername(String username) {
        Customer customer = customerRepository.findByUsername(username);
        customer.setPassword(decryptPassword(customer.getPassword()));
        return customer;
    }

    @Override
    public Iterable<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @Override
    public Customer save(Customer customer) {
        return customerRepository.save(encryptPassword(customer));
    }

    @Override
    public void deleteById(Long customerId) {
        customerRepository.delete(customerId);
    }

    private boolean isPasswordCorrect(UserLogin login) {
        String password = customerRepository.findByUsername(login.getUsername()).getPassword();
        password = decryptPassword(password);
        return password.equals(login.getPassword());
    }

    private Customer encryptPassword(Customer customer) {
        customer.setPassword(
                AESEncryptor.encryptForStringWithCBCMode(
                        customer.getPassword(), AESKeyGenerator.obtainKeyFromFile(AESKeyGenerator.getProdEnvKey()), initialVector));
        return customer;
    }

    private String decryptPassword(String encryptedPassword) {
        return AESDecryptor.decryptForStringWithCBCMode(
                encryptedPassword, AESKeyGenerator.obtainKeyFromFile(AESKeyGenerator.getProdEnvKey()), initialVector);
    }

}
