package com.rga.service;

import com.rga.domain.Customer;
import com.rga.dto.security.UserLogin;

/**
 * @author Carl Lu
 */
public interface CustomerService {

    public boolean isUserAlreadyExist(String userName);

    public boolean isLoginSuccessful(UserLogin login);

    public String getUserRole(String username);

    public Customer getById(Long customerId);

    public Customer getByUsername(String username);

    public Iterable<Customer> getAllCustomers();

    public Customer save(Customer customer);

    public void deleteById(Long customerId);

}
