package com.rga.repository;

import com.rga.domain.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Carl Lu
 */
@Repository
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Long> {

    public Customer findByUsername(String username);

}
