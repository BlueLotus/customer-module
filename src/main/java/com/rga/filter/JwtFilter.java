package com.rga.filter;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.rga.exception.AuthorizationFailedException;
import com.rga.exception.InvalidTokenException;
import com.rga.repository.CustomerRepository;
import com.rga.util.ApplicationKeyUtil;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;

/**
 * @author Carl Lu
 */
@Component
public class JwtFilter extends GenericFilterBean {

    private final String AUTH_TOKEN_HEADER = "Bearer ";

    CustomerRepository customerRepository;

    @Override
    public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse,
            final FilterChain filterChain) throws IOException, ServletException {

        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final String authHeader = request.getHeader("Authorization");

        String method = request.getMethod();
        if (!method.toUpperCase().equals("OPTIONS")) {
            if (customerRepository == null) {
                ServletContext servletContext = servletRequest.getServletContext();
                WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
                customerRepository = webApplicationContext.getBean(CustomerRepository.class);
            }

            if (authHeader == null || !authHeader.startsWith(AUTH_TOKEN_HEADER)) {
                throw new AuthorizationFailedException();
            }

            // The part after "Bearer "
            final String token = authHeader.substring(AUTH_TOKEN_HEADER.length());

            try {
                final Claims claims = Jwts.parser()
                        .setSigningKey(ApplicationKeyUtil.getInstance().getSecretKeyForJWT())
                        .parseClaimsJws(token)
                        .getBody();
                String username = (String) claims.get("username");

                if (!DigestUtils.sha1Hex(token).equals(customerRepository.findByUsername(username).getTokenDigest())) {
                    throw new InvalidTokenException("Token expired or invalid.");
                }

                request.setAttribute("claims", claims);
            } catch (final SignatureException e) {
                throw new InvalidTokenException();
            }

        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

}
