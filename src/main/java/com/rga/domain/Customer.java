package com.rga.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * @author Carl Lu
 */
@Entity
public class Customer {

    @Id
    @GeneratedValue
    @Column(name = "CUSTOMER_ID")
    private Long id;

    @Column(name = "USER_NAME")
    @NotEmpty
    private String username;

    @Column(name = "PASSWORD")
    @NotEmpty
    private String password;

    @Column(name = "CUSTOMER_NAME")
    @NotEmpty
    private String name;

    @Column(name = "ADMIN")
    @NotNull
    private boolean admin;

    @Column(name = "TOKEN_DIGEST")
    private String tokenDigest;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    @JsonIgnore
    public String getTokenDigest() {
        return tokenDigest;
    }

    @JsonProperty
    public void setTokenDigest(String tokenDigest) {
        this.tokenDigest = tokenDigest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (admin != customer.admin) return false;
        if (id != null ? !id.equals(customer.id) : customer.id != null) return false;
        if (username != null ? !username.equals(customer.username) : customer.username != null) return false;
        if (password != null ? !password.equals(customer.password) : customer.password != null) return false;
        if (name != null ? !name.equals(customer.name) : customer.name != null) return false;
        return !(tokenDigest != null ? !tokenDigest.equals(customer.tokenDigest) : customer.tokenDigest != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (admin ? 1 : 0);
        result = 31 * result + (tokenDigest != null ? tokenDigest.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", admin=" + admin +
                ", tokenDigest='" + tokenDigest + '\'' +
                '}';
    }

}
