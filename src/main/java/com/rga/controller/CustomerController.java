package com.rga.controller;

import com.rga.domain.Customer;
import com.rga.exception.ResourceNotFoundException;
import com.rga.exception.UserAlreadyExistException;
import com.rga.service.impl.CustomerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
/**
 * @author Carl Lu
 */
@RestController("customerController")
@RequestMapping("/rga-customer/customer-service/")
public class CustomerController {

    @Autowired
    private CustomerServiceImpl customerService;

    @RequestMapping(value = "/customers", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Customer>> getAllCustomers() {
        Iterable<Customer> customers = customerService.getAllCustomers();
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    @RequestMapping(value = "/customers/{customerId}", method = RequestMethod.GET)
    public ResponseEntity<?> getCustomer(@PathVariable Long customerId) {
        verifyCustomer(customerId);
        Customer customer = customerService.getById(customerId);
        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    @RequestMapping(value = "/customer", method = RequestMethod.POST)
    public ResponseEntity<Void> createCustomer(@RequestBody Customer customer) {
        if(customerService.isUserAlreadyExist(customer.getUsername())) {
            throw new UserAlreadyExistException();
        }
        customer.setTokenDigest("none");
        customer = customerService.save(customer);
        HttpHeaders responseHeaders = new HttpHeaders();
        URI newCustomerUri = composeResponseURI(customer.getId());
        responseHeaders.setLocation(newCustomerUri);
        return new ResponseEntity<>(null, responseHeaders, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/customers/{customerId}", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateCustomer(@RequestBody Customer customer, @PathVariable Long customerId) {
        verifyCustomer(customerId);
        customerService.save(updateIgnoreTokenDigest(customer));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/customers/{customerId}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteCustomer(@PathVariable Long customerId) {
        verifyCustomer(customerId);
        customerService.deleteById(customerId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    protected Customer updateIgnoreTokenDigest(Customer customer) {
        Customer old = customerService.getById(customer.getId());
        customer.setTokenDigest(old.getTokenDigest());
        return customer;
    }

    protected void verifyCustomer(Long customerId) throws ResourceNotFoundException{
        Customer customer = customerService.getById(customerId);
        if(customer == null) {
            throw new ResourceNotFoundException("Customer with id: " + customerId + " not found!");
        }
    }

    protected URI composeResponseURI(Long customerId) {
        return ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(customerId)
                .toUri();
    }

}
