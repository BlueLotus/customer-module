package com.rga.controller;

import com.rga.domain.Customer;
import com.rga.dto.security.LoginResponse;
import com.rga.dto.security.UserLogin;
import com.rga.service.CustomerService;
import com.rga.util.jwt.JWTUtil;
import io.jsonwebtoken.Claims;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Carl Lu
 */
@RestController
@RequestMapping(value = "/rga-customer/")
public class EntryController {

    @Autowired
    CustomerService customerService;

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public ResponseEntity<LoginResponse> login(@RequestBody final UserLogin login) {
        String username = login.getUsername();
        customerService.isLoginSuccessful(login);
        String token = JWTUtil.generateToken(username, customerService.getUserRole(username));
        setTokenDigestForUser(username, DigestUtils.sha1Hex(token));
        return new ResponseEntity<>(new LoginResponse(token), HttpStatus.OK);
    }

    @RequestMapping(value = "logout", method = RequestMethod.GET)
    public ResponseEntity<Void> logout(final HttpServletRequest request) {
        invalidateToken(request);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    protected boolean setTokenDigestForUser(String username, String tokenDigest) {
        Customer customer = customerService.getByUsername(username);
        customer.setTokenDigest(tokenDigest);
        customerService.save(customer);
        return true;
    }

    protected void invalidateToken(final HttpServletRequest request) {
        final Claims claims = (Claims) request.getAttribute("claims");
        String username = (String)claims.get("username");
        Customer customer = customerService.getByUsername(username);
        customer.setTokenDigest("none");
        customerService.save(customer);
    }

}
