package com.rga.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Carl Lu
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class AuthorizationFailedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AuthorizationFailedException() {

    }

    public AuthorizationFailedException(String message) {
        super(message);
    }

    public AuthorizationFailedException(String message, Throwable cause) {
        super(message, cause);
    }

}
