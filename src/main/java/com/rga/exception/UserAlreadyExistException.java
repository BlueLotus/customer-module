package com.rga.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Carl Lu
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserAlreadyExistException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UserAlreadyExistException() {

    }

    public UserAlreadyExistException(String message) {
        super(message);
    }
    
    public UserAlreadyExistException(String message, Throwable cause) {
        super(message, cause);
    }

}
