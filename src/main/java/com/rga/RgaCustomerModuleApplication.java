package com.rga;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RgaCustomerModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(RgaCustomerModuleApplication.class, args);
    }

}
