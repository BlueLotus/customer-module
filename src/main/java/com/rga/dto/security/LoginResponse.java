package com.rga.dto.security;

/**
 * @author Carl Lu
 */
public class LoginResponse {

    public String token;

    public LoginResponse(final String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "token='" + token + '\'' +
                '}';
    }

}
