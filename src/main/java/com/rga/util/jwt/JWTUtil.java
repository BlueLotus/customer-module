package com.rga.util.jwt;

import com.rga.util.ApplicationKeyUtil;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

/**
 * @author Carl Lu
 */
public class JWTUtil {

    // 15 mins
    private static final long TIME_TO_LIVE = 900000l;

    public static String generateToken(String username, String role) {
        Date now = new Date();
        Date expiration = new Date(now.getTime() + TIME_TO_LIVE);
        return Jwts.builder()
                .setSubject(username)
                .claim("role", role)
                .claim("username", username)
                .setIssuedAt(now)
                .signWith(SignatureAlgorithm.HS256, ApplicationKeyUtil.getInstance().getSecretKeyForJWT())
                .setExpiration(expiration)
                .compact();
    }

}
