package com.rga.util;

import com.rga.util.cryp.AESKeyGenerator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Properties;

/**
 * @author Carl Lu
 */
public class ApplicationKeyUtil {

    private static ApplicationKeyUtil instance;
    private static String prodEnvKey = AESKeyGenerator.getProdEnvKey();

    private String secretKeyForJWT;
    private String initialVectorForCryp;
    private byte[] aesKey;

    private ApplicationKeyUtil() {
        this.secretKeyForJWT = generateSecretKeyForJWT();
        this.initialVectorForCryp = obtainInitVector();
        this.aesKey = obtainAesKey();
    }

    public static ApplicationKeyUtil getInstance() {
        if(instance == null) {
            synchronized (ApplicationKeyUtil.class) {
                if (instance == null) {
                    instance = new ApplicationKeyUtil();
                }
            }
        }
        return instance;
    }

    public String getSecretKeyForJWT() {
        return secretKeyForJWT;
    }

    public String getInitialVectorForCryp() {
        return initialVectorForCryp;
    }

    public byte[] getAesKey() {
        return aesKey;
    }

    private String generateSecretKeyForJWT() {
        SecureRandom secureRandom = new SecureRandom();
        return new BigInteger(130, secureRandom).toString(32);
    }

    private String obtainInitVector() {
        Properties properties = new Properties();
        String configFile = "applicationkeyutil.properties";
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFile);

        try {
            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new FileNotFoundException("Property file '" + configFile + "' not found in the classpath.");
            }
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return properties.getProperty("initialVectorForCryp");
    }

    private byte[] obtainAesKey() {
        byte[] aesKey;
        if(AESKeyGenerator.obtainKeyFromFile(prodEnvKey) == null) {
            aesKey = AESKeyGenerator.generateSecretKey();
            AESKeyGenerator.writeKeyToFile(aesKey, prodEnvKey);
        } else {
            aesKey = AESKeyGenerator.obtainKeyFromFile(prodEnvKey);
        }
        return aesKey;
    }

}
