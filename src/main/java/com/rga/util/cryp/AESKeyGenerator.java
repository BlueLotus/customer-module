package com.rga.util.cryp;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.*;
import java.security.NoSuchAlgorithmException;

/**
 * @author Carl Lu
 */
public class AESKeyGenerator {

    private static String PROD_ENV_KEY = "key.prod.properties";
    private static String TEST_ENV_KEY = "key.test.properties";

    public static String getProdEnvKey() {
        return PROD_ENV_KEY;
    }

    public static String getTestEnvKey() {
        return TEST_ENV_KEY;
    }

    public static byte[] generateSecretKey() {
        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance("AES");
            keyGenerator.init(128);
            SecretKey secretKey = keyGenerator.generateKey();
            byte[] key = secretKey.getEncoded();
            return key;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void writeKeyToFile(byte[] key, String keyFileName) {
        try {
            ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(key);
            ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
            FileOutputStream out = new FileOutputStream(keyFileName);
            int c = arrayInputStream.read();
            while(c != -1) {
                arrayOutputStream.write(c);
                c = arrayInputStream.read();
            }
            arrayOutputStream.writeTo(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static byte[] obtainKeyFromFile(String keyFileName) {
        try {
            File file = new File(keyFileName);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
            byte[] bytes = new byte[1];
            while(bufferedInputStream.read(bytes) != -1) {
                arrayOutputStream.write(bytes);
            }
            arrayOutputStream.close();
            bufferedInputStream.close();
            return arrayOutputStream.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
